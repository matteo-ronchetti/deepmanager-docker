FROM ubuntu:latest

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV PATH /opt/conda/bin:$PATH

RUN apt-get update --fix-missing && apt-get install -y --no-install-recommends wget bzip2 ca-certificates git build-essential cmake unzip yasm pkg-config libtbb2 \
    libtbb-dev libjpeg-dev libpng-dev libtiff-dev \
    libavformat-dev \
    libpq-dev \
	libxine2-dev \
	libglew-dev \
	libtiff5-dev \
	zlib1g-dev \
	libavcodec-dev \
	libavformat-dev \
	libavutil-dev \
	libpostproc-dev \
	libswscale-dev \
	libeigen3-dev && rm -rf /var/lib/apt/lists/*

# libswscale-dev

RUN wget --quiet https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda.sh && \
    /bin/bash ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh && \
    ln -s /opt/conda/etc/profile.d/conda.sh /etc/profile.d/conda.sh && \
    echo ". /opt/conda/etc/profile.d/conda.sh" >> ~/.bashrc && \
    echo "conda activate base" >> ~/.bashrc

RUN conda install -y numpy scipy scikit-learn && conda install pytorch-cpu torchvision-cpu faiss-cpu -c pytorch && conda clean --all --yes

ARG OPENCV_VERSION=4.1.0

RUN wget https://github.com/opencv/opencv/archive/$OPENCV_VERSION.zip && unzip $OPENCV_VERSION.zip && rm $OPENCV_VERSION.zip && \
    wget https://github.com/opencv/opencv_contrib/archive/$OPENCV_VERSION.zip  && unzip $OPENCV_VERSION.zip && rm $OPENCV_VERSION.zip && \
    mkdir /opencv-$OPENCV_VERSION/build && cd /opencv-$OPENCV_VERSION/build && cmake -DBUILD_JAVA=OFF -D PYTHON3_LIBRARY=/opt/conda/lib/python3.7 \
    -D PYTHON3_INCLUDE_DIRS=/opt/conda/include/python3.7m \
    -D PYTHON3_EXECUTABLE=/opt/conda/bin/python3.7 \
    -D PYTHON3_PACKAGES_PATH=/opt/conda/lib/python3.7/site-packages \
    -DWITH_OPENGL=OFF -DWITH_OPENCL=OFF -DWITH_QT=OFF \
    -DWITH_IPP=ON -DWITH_TBB=ON -DWITH_EIGEN=ON -DWITH_V4L=ON \
    -DBUILD_JPEG=ON -DBUILD_PNG=ON -DBUILD_TIFF=ON -DBUILD_WEBP=ON \
    -DBUILD_opencv_apps=OFF -DENABLE_PRECOMPILED_HEADERS=OFF \
    -DBUILD_TESTS=OFF -DBUILD_PERF_TESTS=OFF \
    -DWITH_GTK=OFF -DMKL_ROOT_DIR="/opt/conda/pkgs/mkl-2019.3-199" \
    -DCPU_BASELINE=AVX2 \    
    -DCPU_DISPATCH=AVX512 \
    -DOPENCV_EXTRA_MODULES_PATH=../../opencv_contrib-$OPENCV_VERSION/modules -DBUILD_opencv_legacy=OFF \
    -DOPENCV_ENABLE_NONFREE=ON \
    -DCMAKE_BUILD_TYPE=RELEASE  .. && \
    cd /opencv-$OPENCV_VERSION/build && make -j$(nproc --all) install && \
    rm -r /opencv-$OPENCV_VERSION && rm -r /opencv_contrib-$OPENCV_VERSION

