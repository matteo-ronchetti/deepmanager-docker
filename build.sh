#!/usr/bin/env bash

cd base
sudo docker build -t deepmanager/base .
cd ..

cd cv
sudo docker build -t deepmanager/cv .
cd ..

cd nlp
sudo docker build -t deepmanager/nlp .
cd ..

cd pytorch
sudo docker build --build-arg FROM_IMAGE=nlp -t deepmanager/nlp-pytorch .
sudo docker build --build-arg FROM_IMAGE=cv -t deepmanager/cv-pytorch .
cd ..

cd tensorflow
sudo docker build --build-arg FROM_IMAGE=nlp -t deepmanager/nlp-tensorflow .
sudo docker build --build-arg FROM_IMAGE=cv -t deepmanager/cv-tensorflow .
cd ..